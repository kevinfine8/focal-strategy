<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Vehicle;
use App\Feature;
use App\VehicleMake;
use App\VehicleModel;
use DB;


class VehicleTest extends TestCase
{
	use RefreshDatabase;
    /** @test */
    public function it_displays_all_vehicles()
    {
    	$this->withoutExceptionHandling();
        $vehicle=factory(Vehicle::class)->create();
        $feature=factory(Feature::class)->create();
        $vehicle->features()->SyncWithoutDetaching($feature);
        $response = $this->get('/');
        $response->assertStatus(200)->assertSeeText($vehicle->VRM);

    }

    /** @test */
    public function it_filtters_vehicles_by_make()
    {
    	$this->withoutExceptionHandling();
        $vehicles=factory(Vehicle::class, 7)->create();
        $features=factory(Feature::class, 5)->create();

        foreach ($vehicles as $vehicle) {
        	$i=rand(1, 4);
        	for ($x = 0; $x <= $i; $x++) {
        		$randomFeature=$features->random();
        		$vehicle->features()->SyncWithoutDetaching($randomFeature);
        	}
        }
        
        $randomVehicle=$vehicles->random();
        $hiddenVehicle=$vehicles->whereNotIn('make_id', [$randomVehicle->make_id])->first();


        $response = $this->post('filter', ['make_id' => $randomVehicle->make_id]);
        $response->assertStatus(200)
        	->assertSeeText($randomVehicle->VRM)
        	->assertDontSeeText($hiddenVehicle->VRM);
    }

    /** @test */
    public function it_sorts_vehicals()
    {
    	$this->withoutExceptionHandling();
    	$make1=factory(VehicleMake::class)->create(['name' => 'BMW']);
    	$make2=factory(VehicleMake::class)->create(['name' => 'Vauxhall']);
    	$make3=factory(VehicleMake::class)->create(['name' => 'Ford']);

    	$model1=factory(VehicleModel::class)->create(['name' => '5 Series']);
    	$model2=factory(VehicleModel::class)->create(['name' => 'Zafira']);
    	$model3=factory(VehicleModel::class)->create(['name' => 'Mondeo']);

    	$vehicle1=factory(Vehicle::class)->create(['mileage' => '1248', 'make_id' => $make1->id, 'model_id' => $model1->id]);
    	$vehicle2=factory(Vehicle::class)->create(['mileage' => '5000', 'make_id' => $make2->id, 'model_id' => $model2->id]);
        $vehicle3=factory(Vehicle::class)->create(['mileage' => '4000', 'make_id' => $make3->id, 'model_id' => $model3->id]);


        $response = $this->post('sort', ['orderBy' => 'make']);
        $response->assertStatus(200)
        	->assertSeeInOrder([$vehicle1->VRM, $vehicle3->VRM, $vehicle2->VRM]);

        $response = $this->post('sort', ['orderBy' => 'model']);
        $response->assertStatus(200)
        	->assertSeeInOrder([$vehicle1->VRM, $vehicle3->VRM, $vehicle2->VRM]);

        $response = $this->post('sort', ['orderBy' => 'mileage']);
        $response->assertStatus(200)
        	->assertSeeInOrder([$vehicle1->VRM, $vehicle3->VRM, $vehicle2->VRM]);

    }

        /** @test */
    public function it_filters_by_feature()
    {
    	$this->withoutExceptionHandling();
    	$make1=factory(VehicleMake::class)->create(['name' => 'BMW']);
    	$make2=factory(VehicleMake::class)->create(['name' => 'Vauxhall']);
    	$make3=factory(VehicleMake::class)->create(['name' => 'Ford']);

    	$model1=factory(VehicleModel::class)->create(['name' => '5 Series']);
    	$model2=factory(VehicleModel::class)->create(['name' => 'Zafira']);
    	$model3=factory(VehicleModel::class)->create(['name' => 'Mondeo']);

    	$vehicle1=factory(Vehicle::class)->create(['mileage' => '1248', 'make_id' => $make1->id, 'model_id' => $model1->id]);
    	$vehicle2=factory(Vehicle::class)->create(['mileage' => '5000', 'make_id' => $make2->id, 'model_id' => $model2->id]);
        $vehicle3=factory(Vehicle::class)->create(['mileage' => '4000', 'make_id' => $make3->id, 'model_id' => $model3->id]);

        $feature1=factory(Feature::class)->create(['name' => 'Bluetooth']);
        $feature2=factory(Feature::class)->create(['name' => 'Sat Nav']);
        $vehicle1->features()->SyncWithoutDetaching($feature1);
        $vehicle2->features()->SyncWithoutDetaching($feature2);

        $response = $this->post('filter-feature', ['checkedFeatures' => ['0' => $feature1->id]]);
        $response->assertStatus(200)
        	->assertSeeText($feature1->name)
        	->assertDontSeeText($vehicle2->VRM);

    }

        /** @test */
    public function it_displays_vehicles_with_a_combined_features_of_more_than_1500()
    {
    	$this->withoutExceptionHandling();
    	$make1=factory(VehicleMake::class)->create(['name' => 'BMW']);
    	$make2=factory(VehicleMake::class)->create(['name' => 'Vauxhall']);
    	$make3=factory(VehicleMake::class)->create(['name' => 'Ford']);

    	$model1=factory(VehicleModel::class)->create(['name' => '5 Series']);
    	$model2=factory(VehicleModel::class)->create(['name' => 'Zafira']);
    	$model3=factory(VehicleModel::class)->create(['name' => 'Mondeo']);

    	$vehicle1=factory(Vehicle::class)->create(['mileage' => '1248', 'make_id' => $make1->id, 'model_id' => $model1->id]);
    	$vehicle2=factory(Vehicle::class)->create(['mileage' => '5000', 'make_id' => $make2->id, 'model_id' => $model2->id]);
        $vehicle3=factory(Vehicle::class)->create(['mileage' => '4000', 'make_id' => $make3->id, 'model_id' => $model3->id]);

        $feature1=factory(Feature::class)->create(['price' => '1000']);
    	$feature2=factory(Feature::class)->create(['price' => '500']);
    	$feature3=factory(Feature::class)->create(['price' => '200']);

    	$vehicle1->features()->SyncWithoutDetaching($feature1);
    	$vehicle1->features()->SyncWithoutDetaching($feature2);
    	$vehicle1->features()->SyncWithoutDetaching($feature3);
    	$vehicle2->features()->SyncWithoutDetaching($feature2);

        $response = $this->get('/combined-features');
        $response->assertStatus(200)
        	->assertSeeText($vehicle1->VRM)
        	->assertDontSeeText($vehicle2->VRM);

    }
}
