<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Vehicle;
use App\VehicleMake;
use App\Feature;
use Illuminate\Support\Facades\DB;
use App\Filters\UserFilters;

class VehicleController extends Controller
{
    /**
     * Display a listing of the resource.
     *orderBy
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $vehicles = Vehicle::all();
        $vehicleMakes = Vehicle::getAllVehicleMakes();
        $features = Vehicle::getAllFeatures();
        $selectedMakeID=NULL;
        $orderBy = NULL;
        $checkedFeatures = NULL;
        return view('vehicles.index', compact('vehicles', 'vehicleMakes', 'features', 'selectedMakeID', 'orderBy', 'checkedFeatures'));
    }

    public function sortOrder(Request $request)
    {
        $selectedMakeId=$request->make_id;
        $vehicleMakes = Vehicle::getAllVehicleMakes();
        $orderBy = $request->input('orderBy', 'make');
        $vehicles = Vehicle::sortOrder($orderBy);
        $features = Vehicle::getAllFeatures();
        $selectedMakeID=NULL;
        $checkedFeatures = NULL;
        return view('vehicles.index', compact('vehicles', 'vehicleMakes', 'features', 'selectedMakeID', 'orderBy', 'checkedFeatures'));
    }

    public function filterByMake(Request $request)
    {
        $selectedMakeID=$request->make_id;
        $vehicleMakes = Vehicle::getAllVehicleMakes();
        $vehicles = Vehicle::where('make_id', '=', $selectedMakeID)->get();
        $features = Feature::orderBy('name', 'ASC')->get();
        $orderBy = NULL;
        $checkedFeatures = NULL;
        return view('vehicles.index', compact('vehicles', 'vehicleMakes', 'features', 'selectedMakeID', 'orderBy', 'checkedFeatures'));
    }



    public function filterByFeature(Request $request)
    {
        $selectedMakeID=$request->makes;
        $checkedFeatures=$request->checkedFeatures;
        $vehicles = Vehicle::filterByFeature($checkedFeatures);
        $vehicleMakes = Vehicle::getAllVehicleMakes();
        $features = Feature::orderBy('name')->get();
        $orderBy = NULL;
        $checkedFeatures = NULL;
        return view('vehicles.index', compact('vehicles', 'vehicleMakes', 'features', 'selectedMakeID', 'orderBy', 'checkedFeatures'));
    }

    public function combinedFeaturesValue (Request $request)
    {
        $vehicles = Vehicle::combinedFeaturesValue();
        $vehicleMakes = Vehicle::getAllVehicleMakes();
        $features = Vehicle::getAllFeatures();
        $selectedMakeID=NULL;
        $orderBy = NULL;
        $checkedFeatures = NULL;
        return view('vehicles.index', compact('vehicles', 'vehicleMakes', 'features', 'selectedMakeID', 'orderBy', 'checkedFeatures'));
        
    }
}
