<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Vehicle extends Model
{
    private static $sort = [
            "make" => "vehicle_makes.name",
            "model" => "vehicle_makes.name",
            "mileage" => "vehicles.mileage",
        ];

    public static function getAllVehicleMakes(){
        return VehicleMake::orderBy('name')->get();
    }

    public static function getAllFeatures(){
        return Feature::orderBy('name')->get();
    }

    public static function sortOrder($type){
        $vehicles = Vehicle::select('vehicles.*')
            ->join('vehicle_makes', 'vehicle_makes.id', '=', 'vehicles.make_id')
            ->join('vehicle_models', 'vehicle_models.id', '=', 'vehicles.model_id')
            ->orderBy(Vehicle::$sort[$type])
            ->get();
        return $vehicles; 
    }


    public static function combinedFeaturesValue(){
        $vehicles = DB::table('feature_vehicle')
            ->selectRaw('vehicles.VRM, vehicles.id, SUM(features.price) AS total')
            ->join('vehicles', 'vehicles.id', '=', 'feature_vehicle.vehicle_id')
            ->join('features', 'features.id', '=', 'feature_vehicle.feature_id')
            ->groupBy('vehicles.VRM')
            ->groupBy('vehicles.id')
            ->havingRaw('SUM(features.price) > 1500')
            ->get();
        $vehicles = Vehicle::find($vehicles->pluck('id'));
        return $vehicles;
    }

    public static function filterByFeature(array $checkedFeatures){
        $vehicles = Vehicle::whereHas('features', function ($query) use ($checkedFeatures) {
            $query->whereIn('features.id', $checkedFeatures);
        })->get();
        return $vehicles; 
    }

    public function model() {
        return $this->belongsTo(VehicleModel::class);
    }
    public function make() {
        return $this->belongsTo(VehicleMake::class);
    }
    public function features()
    {
        return $this->belongsToMany(Feature::class);
    }
}
