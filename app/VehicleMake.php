<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VehicleMake extends Model
{
    public function vehicles() {
        return $this->hasMany(Vehicle::class);
    }
}
