<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VehicleModel extends Model
{
    public function vehicles() {
        return $this->hasMany(App\Vehicle::class);
    }

}
