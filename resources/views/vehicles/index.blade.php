<!doctype html>

<html lang="en">
<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

  <title>Focal Strategy</title>
</head>
<body>
  <div class="container">
    <h1>Focal Strategy</h1>
    <div class="card mb-3">
      <div class="card-header">Filter by make</div>
      <div class="card-body">
        <form method="POST" action="/filter" id="form">
          {{ csrf_field() }}
          {{ method_field('POST') }}
          <div class="form-group">
            <select name="make_id">
              @foreach ($vehicleMakes as $vehicleMake)
              <option value="{{ $vehicleMake->id }}" @if ($selectedMakeID==$vehicleMake->id) selected="selected" @endif>{{ $vehicleMake->name }}</option>
              @endforeach
            </select>
          </div>
          <button type="submit" class="btn btn-primary btn-sm">Submit</button>
        </form>
      </div>
    </div>


    <div class="card mb-3">
      <div class="card-header">Filter by feature</div>
      <div class="card-body">
        <form method="POST" action="/filter-feature" id="form">
          {{ csrf_field() }}
          {{ method_field('POST') }}
          <div class="form-group">
            @foreach ($features as $feature)
            @php 
            $checked = @in_array($feature->id, $checkedFeatures) ? 'checked="true"' : false;
            @endphp
            <input type="checkbox" name="checkedFeatures[]" value="{{ $feature->id }}" {{  $checked  }}> {{ $feature->name }}
            @endforeach
          </div>
          <button type="submit" class="btn btn-primary btn-sm">Submit</button>

        </form>
      </div>
    </div>

    <form method="POST" action="/sort" id="form">
      {{ csrf_field() }}
      {{ method_field('POST') }}

      <div class="card mb-3">
        <div class="card-header">Order vehicle by</div>
        <div class="card-body">
          <div class="form-group">
            <input type="radio" id="mileage" name="orderBy" value="mileage" @if ($orderBy=='mileage') checked="checked" @endif>
            <label for="mileage">Mileage</label><br>
            <input type="radio" id="make" name="orderBy" value="make" @if ($orderBy=='make') checked="checked" @endif>
            <label for="make">Make</label><br>
            <input type="radio" id="model" name="orderBy" value="model" @if ($orderBy=='model') checked="checked" @endif>
            <label for="model">Model</label>
          </div>
          <button type="submit" class="btn btn-primary btn-sm">Submit</button>

        </div>
      </div>

      <div class="card mb-3">
        <div class="card-body">
          <a href="/combined-features" class="btn btn-primary btn-sm">View combined features of more than £1500</a>
        </div>
      </div>

    </form>
    <table class="table">
      <thead class="thead-dark">
        <tr>
          <th>VRM</th>
          <th>Make</th>
          <th>Model</th>
          <th>Mileage</th>
          <th>Features</th>
        </tr>
      </thead>
      <tbody>
        @foreach ($vehicles as $vehicle)
        <tr>
          <td>{{ $vehicle->VRM }}</td>
          <td>{{ $vehicle->make->name }}</td>
          <td>{{ $vehicle->model->name }}</td>
          <td>{{ $vehicle->mileage }}</td>
          <td>@foreach ($vehicle->features as $feature) {{ $feature->name }}  @endforeach</td>
        </tr>
        @endforeach
      </tbody>
    </table>
  </div>
</body>
</html>