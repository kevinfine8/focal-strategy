<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateForeignKeysFeatures extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('feature_vehicle', function (Blueprint $table) {
            $table->foreign('feature_id')->references('id')->on('features');
            $table->foreign('vehicle_id')->references('id')->on('vehicles');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('feature_vehicle', function (Blueprint $table) {
            $table->dropForeign('feature_vehicle_make_id_foreign');
            $table->dropForeign('feature_vehicle_model_id_foreign');
            });
    }
}
