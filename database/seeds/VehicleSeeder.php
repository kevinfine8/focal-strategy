<?php

use Illuminate\Database\Seeder;

class VehicleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('vehicle_makes')->insert(['name' => 'BMW']);
        DB::table('vehicle_makes')->insert(['name' => 'Ford']);
        DB::table('vehicle_makes')->insert(['name' => 'Vauxhall']);

        DB::table('vehicle_models')->insert(['name' => '3 Series']);
        DB::table('vehicle_models')->insert(['name' => '5 Series']);
        DB::table('vehicle_models')->insert(['name' => 'Mondeo']);
        DB::table('vehicle_models')->insert(['name' => 'Transit']);
        DB::table('vehicle_models')->insert(['name' => 'Zafira,']);

        DB::table('features')->insert(['name' => 'Electric Windows', 'price' => '150']);
        DB::table('features')->insert(['name' => 'Bluetooth', 'price' => '250']);
        DB::table('features')->insert(['name' => 'Sat Nav', 'price' => '1100']);
        DB::table('features')->insert(['name' => 'All Wheel Drive', 'price' => '2000']);
        DB::table('features')->insert(['name' => 'Sliding Side Door', 'price' => '1300']);

        DB::table('vehicles')->insert([
            'VRM' => 'SK14PBF',
            'make_id' => 1,
            'model_id' => 1,
            'mileage' => '40013',
        ]);
        DB::table('vehicles')->insert([
            'VRM' => 'SB17ABC',
            'make_id' => 1,
            'model_id' => 1,
            'mileage' => '12293',
        ]);

        DB::table('vehicles')->insert([
            'VRM' => 'SA63DEF',
            'make_id' => 1,
            'model_id' => 2,
            'mileage' => '66323',
        ]);

        DB::table('vehicles')->insert([
            'VRM' => 'SB14DFF',
            'make_id' => 2,
            'model_id' => 3,
            'mileage' => '44124',
        ]);

        DB::table('vehicles')->insert([
            'VRM' => 'SB66ABF',
            'make_id' => 2,
            'model_id' => 4,
            'mileage' => '88933',
        ]);

        DB::table('vehicles')->insert([
            'VRM' => 'WG12ARB',
            'make_id' => 3,
            'model_id' => 5,
            'mileage' => '32033',
        ]);

        DB::table('vehicles')->insert([
            'VRM' => 'AG12BEF',
            'make_id' => 3,
            'model_id' => 5,
            'mileage' => '48742',
        ]);

        DB::table('feature_vehicle')->insert(['feature_id' => '1', 'vehicle_id' => '1']);
        DB::table('feature_vehicle')->insert(['feature_id' => '2', 'vehicle_id' => '1']);
        DB::table('feature_vehicle')->insert(['feature_id' => '3', 'vehicle_id' => '1']);

        DB::table('feature_vehicle')->insert(['feature_id' => '1', 'vehicle_id' => '2']);
        DB::table('feature_vehicle')->insert(['feature_id' => '2', 'vehicle_id' => '2']);
        DB::table('feature_vehicle')->insert(['feature_id' => '3', 'vehicle_id' => '2']);
        DB::table('feature_vehicle')->insert(['feature_id' => '4', 'vehicle_id' => '2']);

        DB::table('feature_vehicle')->insert(['feature_id' => '1', 'vehicle_id' => '3']);
        DB::table('feature_vehicle')->insert(['feature_id' => '2', 'vehicle_id' => '3']);
        DB::table('feature_vehicle')->insert(['feature_id' => '3', 'vehicle_id' => '3']);

        DB::table('feature_vehicle')->insert(['feature_id' => '1', 'vehicle_id' => '4']);
        DB::table('feature_vehicle')->insert(['feature_id' => '2', 'vehicle_id' => '4']);

        DB::table('feature_vehicle')->insert(['feature_id' => '1', 'vehicle_id' => '5']);
        DB::table('feature_vehicle')->insert(['feature_id' => '2', 'vehicle_id' => '5']);
        DB::table('feature_vehicle')->insert(['feature_id' => '5', 'vehicle_id' => '5']);

        DB::table('feature_vehicle')->insert(['feature_id' => '1', 'vehicle_id' => '6']);

        DB::table('feature_vehicle')->insert(['feature_id' => '1', 'vehicle_id' => '7']);



    }
}
