<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;
use App\VehicleMake;

$factory->define(VehicleMake::class, function (Faker $faker) {
	$faker->addProvider(new \MattWells\Faker\Vehicle\Provider($faker));
    return [
        'name' => $faker->vehicleMake,
    ];
});
