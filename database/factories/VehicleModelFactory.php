<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;
use App\VehicleModel;


$factory->define(VehicleModel::class, function (Faker $faker) {
	$faker->addProvider(new \MattWells\Faker\Vehicle\Provider($faker));
    return [
        'name' => $faker->vehicleModel,
    ];
});
