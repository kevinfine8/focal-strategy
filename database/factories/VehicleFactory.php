<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;
use App\Vehicle;
use App\VehicleMake;
use App\VehicleModel;



$factory->define(Vehicle::class, function (Faker $faker) {
	$faker->addProvider(new \MattWells\Faker\Vehicle\Provider($faker));
	$vehicleMake=factory(VehicleMake::class)->create();
	$vehicleModel=factory(VehicleModel::class)->create(['name'=>$faker->vehicleModel($vehicleMake->name)]);

    return [
            'VRM' => $faker->vehicleRegistration,
            'mileage' => $faker->randomDigit,
            'make_id' => $vehicleMake->id,
            'model_id' => $vehicleModel->id,
    ];
});
