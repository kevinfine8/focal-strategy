<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'VehicleController@index');
Route::post('/filter', 'VehicleController@filterByMake');
Route::post('/sort', 'VehicleController@sortOrder');
Route::post('/filter-feature', 'VehicleController@filterByFeature');
Route::get('/combined-features', 'VehicleController@combinedFeaturesValue');

